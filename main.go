package main

import (
	"fmt"
	"log"
	"strings"
)

func Chunker(s string) []string {
	r := s
	start := 0
	diff := 46
	chunkNum := 0
	var chunkd []string
	if len(r) <= diff {
		return []string{r}
	}

	for len(r) > 0 {

		if chunkNum >= 9 && diff != 45 {
			diff = 45
			r = s
			chunkd = []string{}
			chunkNum = 0
		} else if chunkNum >= 9 && diff == 45 {
			diff = 44
		}

		end := start + diff
		if len(r) <= diff {
			chunkd = append(chunkd, r)
			break
		}

		d := r[start:end]

		if string(d[end-1]) == " " || (end < len(r) && string(r[end]) == " ") {

			chunkd = append(chunkd, strings.Trim(d, " "))

			chunkNum = chunkNum + 1
			r = r[end:]
			log.Println(r)

		} else {

			e := strings.LastIndex(d, " ")

			d = r[start : start+e]
			chunkd = append(chunkd, d)

			chunkNum = chunkNum + 1
			r = strings.Trim(r[start+e:], " ")

		}
	}

	for i, val := range chunkd {

		chunkd[i] = fmt.Sprintf("%d/%d ", i+1, len(chunkd)) + val
	}
	return chunkd
}

func main() {
	s := "I can’t believe Tweeter now supports chunking my messages, so I don’t have to do it myself"
	log.Println(Chunker(s))
}
